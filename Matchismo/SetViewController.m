//
//  SetViewController.m
//  Matchismo
//
//  Created by Mike Halliday on 2/10/13.
//  Copyright (c) 2013 Mike Halliday. All rights reserved.
//

#import "SetViewController.h"
#import "SetCardDeck.h"
#import "SetCard.h"

@interface SetViewController ()

@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *MikeCardButtons;
@property (weak, nonatomic) IBOutlet UILabel *SetsFoundLabel;
@property (weak, nonatomic) IBOutlet UILabel *StatusLabel;
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalLabel;
@end

@implementation SetViewController

-(SetGameMatchingGame*)setGame{
    if (! _setGame)
    {
        //instantialize
        SetCardDeck * newDeck = [[SetCardDeck alloc] initWithSetCards];
        [newDeck shuffleDeck];
        _setGame = [[SetGameMatchingGame alloc] initUsingDeck:newDeck];
    }
    return _setGame;
}

-(void)setMikeCardButtons:(NSArray *)MikeCardButtons
{
    _MikeCardButtons = MikeCardButtons;
    int index = 0;
    for (UIButton* cardButton in MikeCardButtons) {
        SetCard * theCard = [[self.setGame dealOutNumOfCards:1] lastObject];
        [cardButton setAttributedTitle:theCard.display forState:UIControlStateNormal];
        [cardButton setBackgroundImage:[UIImage imageNamed:@"Default.png"] forState:UIControlStateSelected];
        [cardButton setBackgroundImage:[UIImage imageNamed:@"playing-card-front.jpg"] forState:UIControlStateNormal];
        [cardButton setSelected:NO];
        index++;
    }
}

-(void)viewWillDisappear:(BOOL)animated
{
    [self DealButtonPressed];
}

- (IBAction)DealButtonPressed {
    self.setGame = nil;
    [self setMikeCardButtons:self.MikeCardButtons];
    self.SetsFoundLabel.text = [NSString stringWithFormat:@"Sets Found: %i", ([self.setGame.setsFound count]/6)];
    self.scoreLabel.text = [NSString stringWithFormat:@"Score: %i", self.setGame.score];
    [self labelStatus];
    [self UpdateSetUI];
}

-(void) UpdateSetUI
{
    for (UIButton* cardButton in self.MikeCardButtons) {
        cardButton.alpha = 1.0;
        if ([[self.setGame.deckInPlay.cards objectAtIndex:[self.MikeCardButtons indexOfObject:cardButton]] isSelected]){
            cardButton.selected = YES;
        }else{
            cardButton.selected = NO;
        }
    }
    
    for (NSNumber *theNum in self.setGame.invalids) {
        [self.MikeCardButtons[[theNum intValue]] setAlpha:0.1];
    }
    
    self.SetsFoundLabel.text = [NSString stringWithFormat:@"Sets Found: %i", ([self.setGame.setsFound count]/6)];
    self.scoreLabel.text = [NSString stringWithFormat:@"Score: %i", self.setGame.score];
}

- (IBAction)SetCardPressed:(UIButton*)sender {
    self.StatusLabel.text = @"";
    int index = [self.MikeCardButtons indexOfObject:sender];

    SetCard * theCardPressed = [self.setGame.deckInPlay.cards objectAtIndex:index];
    theCardPressed.selected = ! theCardPressed.isSelected;
    if (theCardPressed.isSelected)
    {
        NSString * bah = [self.setGame addCardandCompare:theCardPressed];
        if (! [bah isEqualToString:@"" ]){
            self.StatusLabel.text = bah;}
    }else{
        [self.setGame removeCardFromChecks:theCardPressed];
    }
    [self UpdateSetUI];
}

-(void)viewDidAppear:(BOOL)animated
{
    [self labelStatus];
}

-(void)labelStatus{
    int num = [self.setGame countSetsInDeck];
    self.StatusLabel.text = [NSString stringWithFormat:@"%i Sets to Find",num];
    self.totalLabel.text = [NSString stringWithFormat:@"Total Sets: %i", num];
    }

@end
