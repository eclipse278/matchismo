
//
//  SetViewController.h
//  Matchismo
//
//  Created by Mike Halliday on 2/10/13.
//  Copyright (c) 2013 Mike Halliday. All rights reserved.
//

#import "ViewController.h"
#import "SetGameMatchingGame.h"
@interface SetViewController : ViewController

@property (strong, nonatomic) SetGameMatchingGame * setGame;

@end
