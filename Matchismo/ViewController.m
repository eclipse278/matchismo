//
//  ViewController.m
//  Matchismo
//
//  Created by Mike Halliday on 2/5/13.
//  Copyright (c) 2013 Mike Halliday. All rights reserved.
//

#import "ViewController.h"
#import "PlayingCardMatchingGame.h"
//#import "Deck.h"

@interface ViewController ()
@property (strong, nonatomic) PlayingCardMatchingGame * game;
@property (nonatomic) NSUInteger numOfClicks;
@property (weak, nonatomic) IBOutlet UILabel *clicksLabel;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *cardButtons;
@property (weak, nonatomic) IBOutlet UILabel *ScoreLabel;
- (IBAction)dealButton:(UIButton *)sender;

@end

@implementation ViewController

-(void) setCardButtons:(NSArray *)cardButtons
{
    _cardButtons = cardButtons;
    for (UIButton* Butt in cardButtons) {
        Card * aCard = [[self.game dealOutNumOfCards:1] lastObject];
        [Butt setTitle:aCard.face forState:UIControlStateSelected];

        [Butt setBackgroundImage:[UIImage imageNamed:@"playing-card-back.jpg"] forState:UIControlStateNormal];
        [Butt setBackgroundImage:[UIImage imageNamed:@"playing-card-front.jpg"] forState:UIControlStateSelected];
        [Butt setTitle:@"" forState:UIControlStateNormal];
        
        CGRect buttonFrame = Butt.frame;
        buttonFrame.size = CGSizeMake(61, 86);
        Butt.frame = buttonFrame;        
    }
}

-(void) UpdateUI
{
    for (UIButton* cardButton in self.cardButtons) {
        Card * theCard = [self.game.deckInPlay showCardatIndex:[self.cardButtons indexOfObject:cardButton]];
        [cardButton setTitle:theCard.face forState:UIControlStateSelected|UIControlStateDisabled];

        cardButton.selected = theCard.isFaceUp;
        
        if (! theCard.isPlayable)
        {
            cardButton.enabled = NO;
            [cardButton setBackgroundImage:[UIImage imageNamed:@"playing-card-front.jpg"] forState:UIControlStateNormal];
            cardButton.alpha = 0.99;
        }else{
            cardButton.enabled = YES;
            cardButton.alpha = 1.0;
        }
        _ScoreLabel.text = [NSString stringWithFormat:@"Score: %d", self.game.score];
    }
}
-(PlayingCardMatchingGame*) game
{
    if (! _game){
        Deck * initDeck = [[Deck alloc] initWithPlayingCards];
        [initDeck shuffleDeck];
        _game = [[PlayingCardMatchingGame alloc] initUsingDeck:initDeck];
    }
    return _game;
}

- (IBAction)CardClicked:(UIButton*)sender {
    self.numOfClicks++;
    _clicksLabel.text = [NSString stringWithFormat:@"Flips: %i", self.numOfClicks];
    sender.selected = !sender.isSelected;
    [self.game flipCardatIndex:[self.cardButtons indexOfObject:sender]];
    [self UpdateUI];
}
- (IBAction)dealButton:(UIButton *)sender {
    Deck * restartDeck = [[Deck alloc] initWithPlayingCards];
    [restartDeck shuffleDeck];
    [self.game restartWithDeck:restartDeck];
    [self setCardButtons:self.cardButtons];
    _numOfClicks = 0;
    _clicksLabel.text = [NSString stringWithFormat:@"Flips: 0"];
    [self UpdateUI];}

-(void)viewWillAppear:(BOOL)animated
{
    [self dealButton:[[UIButton alloc]init]];
}


@end