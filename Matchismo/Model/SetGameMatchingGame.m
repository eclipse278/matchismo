//
//  SetGameMatchingGame.m
//  Matchismo
//
//  Created by Mike Halliday on 2/11/13.
//  Copyright (c) 2013 Mike Halliday. All rights reserved.
//

#import "SetGameMatchingGame.h"
//#import "SetCardDeck.h"

@interface SetGameMatchingGame ()
    @property (nonatomic, strong) NSMutableArray * cardsToCompare;
    @property (nonatomic, readwrite) NSMutableArray * setsFound;
    @property (readwrite, nonatomic) int score;
@end

@implementation SetGameMatchingGame

@synthesize score;

-(NSMutableArray*)setsFound
{
    if (! _setsFound){
        _setsFound = [[NSMutableArray alloc]init];
    }
    return _setsFound;
}

- (int) countSetsInDeck
{
    int numofresults = 0;
    
    NSMutableString * result = [[NSMutableString alloc]init];
    
    SetCard * firstCard = [[SetCard alloc]init];
    SetCard * secondCard = [[SetCard alloc]init];
    SetCard * thirdCard = [[SetCard alloc]init];
    
    //search for sets in the dealt cards
    
    for (int firstNum = 0; firstNum < [self.deckInPlay count]; firstNum++)
    {
        firstCard = [self.deckInPlay showCardatIndex:firstNum];
        for (int secondNum = (firstNum+1); secondNum < [self.deckInPlay count]; secondNum++)
        {
            secondCard = [self.deckInPlay showCardatIndex:secondNum];
            for(int thirdNum = (secondNum+1); thirdNum<[self.deckInPlay count]; thirdNum++)
            {
                thirdCard = [self.deckInPlay showCardatIndex:thirdNum];
                result = [NSMutableString stringWithString:[firstCard checkIfMatchWithOtherSetCards:secondCard secondCard:thirdCard]];
                if ( [result isEqualToString:@"Found a set!"])
                {
                    numofresults++;
        //            NSLog(@"Found a set: %@,%@,%@", firstCard.face, secondCard.face, thirdCard.face);
                }
            }
        }
    }
    
    return (numofresults / 1);
}

-(NSMutableArray*)cardsToCompare{
    
    if (!_cardsToCompare)
        _cardsToCompare = [[NSMutableArray alloc]init];
    return _cardsToCompare;
}

-(NSArray*) invalids  
{
    if (! _invalids){
        _invalids = [[NSArray alloc]init];
    }
    return _invalids;
}

- (NSString*) addCardandCompare:(SetCard*)newCard{

    [self.cardsToCompare addObject:newCard];  //add to array
    
    NSMutableString * result = [[NSMutableString alloc]initWithString:@"?"];   //just in case

    if ([self.cardsToCompare count] == 2)
    {
        self.invalids = [self invalidIndexes];  //check for matches with these two cards and set the "invalids" property for the VC to read and blank cards
    }else{
        [self setInvalids:nil];
    }
    
    if ([self.cardsToCompare count] == 3) {     //check for sets
        
        NSArray * checkArray = @[newCard, self.cardsToCompare[0], self.cardsToCompare[1]];
        
        if (! [self.setsFound containsObject:checkArray])
        {
            // not in found array, so let's check it
            result = [NSMutableString stringWithString:[newCard checkIfMatchWithOtherSetCards:[self.cardsToCompare objectAtIndex:0] secondCard:[self.cardsToCompare  objectAtIndex:1]]];
            if ([result isEqualToString:@"Found a set!"])
            {
                //don't know how to sort arrays yet so just add all possibilites to the found sets array
                
                [self.setsFound addObject:@[newCard, [self.cardsToCompare objectAtIndex:0], [self.cardsToCompare objectAtIndex:1]]];
                [self.setsFound addObject:@[[self.cardsToCompare objectAtIndex:0], [self.cardsToCompare objectAtIndex:1], newCard]];
                [self.setsFound addObject:@[[self.cardsToCompare objectAtIndex:0], newCard ,[self.cardsToCompare objectAtIndex:1]]];
            
                [self.setsFound addObject:@[newCard, [self.cardsToCompare objectAtIndex:1], [self.cardsToCompare objectAtIndex:0]]];
                [self.setsFound addObject:@[[self.cardsToCompare objectAtIndex:1], [self.cardsToCompare objectAtIndex:0], newCard]];
                [self.setsFound addObject:@[[self.cardsToCompare objectAtIndex:1], newCard ,[self.cardsToCompare objectAtIndex:0]]];

                self.score += 10;
            }else{
                self.score -= 7;
            }
        }else{
            result = [NSMutableString stringWithString: @"Already found this set!"];
        }
        //  for the VC, deselect cards
        for (SetCard* Card in self.cardsToCompare) {
            Card.selected = NO;
        }
        [self removeAllCardFromChecks];     //and remove them from the array to check
    }else{
        result = [NSMutableString stringWithString:@""];
    }
    return [result copy];
}

- (NSArray*) invalidIndexes  //for alpha'ing the cards for sets we already found
{
    NSMutableArray * invalidArray = [[NSMutableArray alloc]init];
    int index = 0;
    
    if ( [self.cardsToCompare count] == 2)
    {
        for (Card* Card in self.deckInPlay.cards) {
            if ([self.setsFound containsObject:@[self.cardsToCompare[0], self.cardsToCompare[1], Card] ])
            {
                [invalidArray addObject: [NSNumber numberWithInt: index]];
            }
            index++;
        }
    }
    return [invalidArray copy];
}

-(int)score{

    if (! score){
        score = 0;
    }
    
    return score;
}

- (void) removeCardFromChecks:(SetCard*)newCard
{
    [self.cardsToCompare removeObjectIdenticalTo:newCard];
    [self setInvalids:nil];
}
- (void) removeAllCardFromChecks{
    [self.cardsToCompare removeAllObjects];
    [self setInvalids:nil];
}

@end
