//
//  NewDeck.m
//  Matchismo
//
//  Created by Mike Halliday on 2/5/13.
//  Copyright (c) 2013 Mike Halliday. All rights reserved.
//

#import "Deck.h"
#import "Card.h"
#import "PlayingCard.h"

@interface Deck ()
@property (readwrite, nonatomic) NSMutableArray * cards;
@end

@implementation Deck

-(id)init{
    
    self = [super init];
    
    if (self)
    {
        _cards = [[NSMutableArray alloc]init];
    }
    
    return self;
}

-(id)initFromArrayofCards:(NSArray*)theArray{
    self = [self init];
    
    if (self)
    {
        self.cards = [[NSMutableArray alloc] initWithCapacity:[theArray count]];
        int counter = 0;
        for (Card * theCard in theArray) {
            [self.cards addObject:[theArray objectAtIndex:counter]];
            counter++;
        }
    }
    return self;
}

- (id) initWithPlayingCards {
    self = [self init];
    
    if (self)
    {
        self.cards = [[NSMutableArray alloc] initWithCapacity:52];
        for (int i=1;i<14;i++)
        {
            [self.cards addObject:[[PlayingCard alloc]initWithRank:i Suit:@"♦" ]];
            [self.cards addObject:[[PlayingCard alloc]initWithRank:i Suit:@"♥" ]];
            [self.cards addObject:[[PlayingCard alloc]initWithRank:i Suit:@"♠" ]];
            [self.cards addObject:[[PlayingCard alloc]initWithRank:i Suit:@"♣" ]];
        }
    }
    return self;
}

- (void) shuffleDeck{
    for (NSInteger i = self.cards.count-1; i > 0; i--)
    {
        [self.cards exchangeObjectAtIndex:i withObjectAtIndex:arc4random_uniform(i+1)];
    }
}

-(int) count
{
    return self.cards.count;
}

- (id)DrawCard {
    Card *theCard = [[Card alloc] init];
    
    theCard = [self.cards lastObject];
    [self.cards removeLastObject];
        
    return theCard;
}

-(id)showCardatIndex:(int)index
{
    return [self.cards objectAtIndex:index];
}

-(void) addCard:(Card*)theCard atIndex:(int)index{
    [self.cards insertObject:theCard atIndex:index];
}

-(NSString*)desc
{
    NSMutableString * returnString = [[NSMutableString alloc] init];
    for (Card * theCard in self.cards) {
        [returnString appendFormat:@"%@, ", theCard.face];
    }
    return [NSString stringWithString:returnString];
}

@end
