//
//  Card.m
//  Matchismo
//
//  Created by Mike Halliday on 2/5/13.
//  Copyright (c) 2013 Mike Halliday. All rights reserved.
//

#import "Card.h"

@implementation Card

- (int) checkIfMatchWith: (Card*) otherCard{
    NSLog(@"Entered Generic Card checkif match with");

    if ([self.face isEqualToString: otherCard.face]){
        return 1;
    }else{
        return 0;
    }
}

-(void)flipCard
{
    self.faceUp = ! self.faceUp;
    
//    if (self.isFaceUp)
//        NSLog(@"%@ is now faceUp", self.face);
//    else
//        NSLog(@"%@ is now faceDown", self.face);
}

-(void)setPlayable:(BOOL)plaable{
    _playable = plaable;
}

@end
