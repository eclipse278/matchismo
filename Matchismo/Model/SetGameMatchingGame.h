//
//  SetGameMatchingGame.h
//  Matchismo
//
//  Created by Mike Halliday on 2/11/13.
//  Copyright (c) 2013 Mike Halliday. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CardMatchingGame.h"
#import "SetCard.h"

@interface SetGameMatchingGame : CardMatchingGame

@property (nonatomic, strong) NSArray * invalids;
@property (nonatomic, readonly) NSMutableArray * setsFound;

- (int) countSetsInDeck;
- (NSString*) addCardandCompare:(SetCard*)newCard;
- (void) removeCardFromChecks:(SetCard*)newCard;
- (void) removeAllCardFromChecks;
-(NSArray*) invalidIndexes;
@end
