//
//  CardMatchingGame.h
//  Matchismo
//
//  Created by Mike Halliday on 2/11/13.
//  Copyright (c) 2013 Mike Halliday. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Deck.h"

@interface CardMatchingGame : NSObject

@property (readonly, nonatomic) int score;
@property (strong, nonatomic) Deck * deck;
@property (strong, nonatomic) Deck * deckInPlay;
@property (strong, nonatomic) Deck * discarded;

- (id) initUsingDeck:(Deck*)initingDeck;
-(void) restartWithDeck:(Deck*)newDeck;

//deal some out
-(NSArray*) dealOutNumOfCards:(int)numofcards;

//flip a card
-(void) flipCardatIndex:(int)index;

//check a card
-(BOOL)isCardPlayableAtIndex:(int)index;

//see if they match and award points
-(int) compareCard: (Card*)cardOne withCard:(Card*)cardTwo;

@end
