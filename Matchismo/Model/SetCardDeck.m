//
//  SetCardDeck.m
//  Matchismo
//
//  Created by Mike Halliday on 2/10/13.
//  Copyright (c) 2013 Mike Halliday. All rights reserved.
//

#import "SetCardDeck.h"
#import "SetCard.h"

@interface SetCardDeck ()
@end

@implementation SetCardDeck

-(id)initWithSetCards
{
    self = [self init];
    
    if (self){

        //add cards to deck
        
        NSArray * shapes = @[@"●",@"■",@"▲"];
        
        SetCard * theCard = [[SetCard alloc]init];
        
        for (int number=1;number<4;number++)
        {
            for (int colornum=0;colornum<3;colornum++)
            {
                for (int shapenum=0;shapenum<3;shapenum++)
                {
                    for (int fillnum=0;fillnum<3;fillnum++)
                    {
                        theCard = [[SetCard alloc]initWithChar:shapes[shapenum] Color:colornum Fill:fillnum andNumber:number];
                        [self.cards addObject:theCard];
                    }
                }
            }
        }
    }
    
    
    return self;
}


@end
