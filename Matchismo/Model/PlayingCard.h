//
//  PlayingCard.h
//  Matchismo
//
//  Created by Mike Halliday on 2/5/13.
//  Copyright (c) 2013 Mike Halliday. All rights reserved.
//

#import "Card.h"

@interface PlayingCard : Card

// Playing Cards are type Card but what extra properties do they have?

@property (nonatomic, strong) NSString * rank;
@property (nonatomic, strong) NSString * suit;

//What can you do with a Playing Card that you can't do with a normal card?

- (id) initWithRank:(NSUInteger)inti Suit:(NSString*)strang;
- (int) checkIfMatchWith: (PlayingCard *) otherCard;

@end
