//
//  PlayingCard.m
//  Matchismo
//
//  Created by Mike Halliday on 2/5/13.
//  Copyright (c) 2013 Mike Halliday. All rights reserved.
//

#import "PlayingCard.h"
#import "Card.h"

@implementation PlayingCard

@synthesize rank=_rank;
@synthesize suit=_suit;

- (id) initWithRank:(NSUInteger)inti Suit:(NSString*)strang{
    
    self = [super init];
    
    if ((self) && (inti < 14)){
        _rank = [NSString stringWithFormat:@"%i", inti];

        if (inti == 1)
        {
            _rank = @"A";
        }else if (inti == 11){
            _rank = @"J";
        }else if (inti == 12) {
            _rank = @"Q";
        }else if (inti == 13){
            _rank = @"K";
        }
        _suit = strang;
        [self setFace: [NSString stringWithFormat:@"%@%@", _rank, _suit]];
        self.playable = YES;
    }
    return self;
}

-(int) checkIfMatchWith:(PlayingCard *)otherCard
{
//    NSLog(@"Entered Playing Card checkif match with");
    int points = 0;
    if ([self.rank isEqualToString:otherCard.rank])
    {
        points = 13;
//        NSLog(@"%@ matched rank with %@", [self face], [otherCard face]);
    } else if ([self.suit isEqualToString:otherCard.suit]){
//        NSLog(@"%@ matched suit with %@", [self face], [otherCard face]);
        points = 5;
    }
    return points;
}
@end
