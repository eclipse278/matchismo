//
//  SetCard.h
//  Matchismo
//
//  Created by Mike Halliday on 2/10/13.
//  Copyright (c) 2013 Mike Halliday. All rights reserved.
//

#import "Card.h"

@interface SetCard : Card

//What properties does a Set Card Have?

@property (strong, nonatomic) NSAttributedString * display;
@property (strong, nonatomic) NSString * character;     //any character
@property (nonatomic) int color;                        //green(0) ,blue (1), red (2)
@property (nonatomic) int fill;                         //empty (0), shaded (1), or filled (2)
@property (nonatomic) NSUInteger number;                //1,2,3
@property (nonatomic, getter = isSelected) BOOL selected;                //T or F (tapped)

//What can you do with a Set Card that can't be done with normal cards?

-(id) initWithChar:(NSString*)chctr Color:(int)color Fill:(int)fill andNumber:(NSUInteger)numOfChar;
- (NSString*) checkIfMatchWithOtherSetCards:(SetCard *)firstCard secondCard:(SetCard*)secondCard;

@end
