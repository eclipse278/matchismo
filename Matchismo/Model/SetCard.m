//
//  SetCard.m
//  Matchismo
//
//  Created by Mike Halliday on 2/10/13.
//  Copyright (c) 2013 Mike Halliday. All rights reserved.
//

#import "SetCard.h"

@implementation SetCard



#define FONT_SIZE 17.0

-(id)initWithChar:(NSString *)chctr Color:(int)color Fill:(int)fill andNumber:(NSUInteger)numOfChar
{
    
    SetCard * Card = [[SetCard alloc] init];

    Card.faceUp = YES;
    NSMutableString * cardFace = [[NSMutableString alloc]initWithCapacity:3];
    
    // set up the characters string
    
    int lengthofcardface=0;
    for (int r=0;r<numOfChar;r++) {
        [cardFace appendString:chctr];
        lengthofcardface++;
    }
    
    //build the card's attributed string
    
    NSMutableAttributedString * setCardString = [[NSMutableAttributedString alloc]initWithString:cardFace];
    [setCardString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:FONT_SIZE] range:NSMakeRange(0, lengthofcardface)];
    [setCardString addAttribute:NSStrokeWidthAttributeName value:@-10 range:NSMakeRange(0, lengthofcardface)];

    switch (color) {
        case 0:
            switch (fill) {
                case 0:  //empty
                    [setCardString addAttribute:NSForegroundColorAttributeName value:[[UIColor alloc]initWithWhite:255.0 alpha:0.0 ]range:NSMakeRange(0, lengthofcardface)];
                    break;
                case 1: // transparent
                    [setCardString addAttribute:NSForegroundColorAttributeName value:[[UIColor alloc]initWithRed:0.0 green:255.0 blue:0.0 alpha:0.2] range:NSMakeRange(0, lengthofcardface)];
                    break;
                case 2:// solid
                    [setCardString addAttribute:NSForegroundColorAttributeName value:[UIColor greenColor] range:NSMakeRange(0, lengthofcardface)];
                    break;
                default:
                    [setCardString addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(0, lengthofcardface)];
                    break;
            }
            [setCardString addAttribute:NSStrokeColorAttributeName value:[UIColor greenColor] range:NSMakeRange(0, lengthofcardface)];
            break;
        case 1:
            switch (fill) {
                case 0:  //empty
                    [setCardString addAttribute:NSForegroundColorAttributeName value:[[UIColor alloc]initWithWhite:255.0 alpha:0.0 ]range:NSMakeRange(0, lengthofcardface)];
                    break;
                case 1: // transparent
                    [setCardString addAttribute:NSForegroundColorAttributeName value:[[UIColor alloc]initWithRed:0.0 green:0.0 blue:255.0 alpha:0.2] range:NSMakeRange(0, lengthofcardface)];
                    break;
                case 2:// solid
                    [setCardString addAttribute:NSForegroundColorAttributeName value:[UIColor blueColor] range:NSMakeRange(0, lengthofcardface)];
                    break;
                default:
                    [setCardString addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(0, lengthofcardface)];

                    break;
            }
            [setCardString addAttribute:NSStrokeColorAttributeName value:[UIColor blueColor] range:NSMakeRange(0, lengthofcardface)];
            break;
        case 2 :
            switch (fill) {
                case 0:  //empty
                    [setCardString addAttribute:NSForegroundColorAttributeName value:[[UIColor alloc]initWithWhite:255.0 alpha:0.0 ]range:NSMakeRange(0, lengthofcardface)];
                    break;
                case 1: // transparent
                    [setCardString addAttribute:NSForegroundColorAttributeName value:[[UIColor alloc]initWithRed:255.0 green:0.0 blue:0.0 alpha:0.2] range:NSMakeRange(0, lengthofcardface)];
                    break;
                case 2:// solid
                    [setCardString addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(0, lengthofcardface)];
                    break;
                default:
                    [setCardString addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(0, lengthofcardface)];
                    break;
            }
            [setCardString addAttribute:NSStrokeColorAttributeName value:[UIColor redColor] range:NSMakeRange(0, lengthofcardface)];
            break;
        default:
            [setCardString addAttribute:NSForegroundColorAttributeName value:[UIColor orangeColor] range:NSMakeRange(0, lengthofcardface)];
            break;
    }

    //set card properties for checking.  Realized now Hagerty probably wanted me to check the attrib string's properties but i'm in too deep now.
    Card.face = [NSString stringWithFormat:@"num:%i fill:%i color:%i, shape:%@", numOfChar,fill,color,chctr];
    Card.display = [setCardString copy];
    Card.color = color;
    Card.character = chctr;
    Card.fill = fill;
    Card.number = numOfChar;
    
    return Card;
}
- (NSString*) checkIfMatchWithOtherSetCards:(SetCard *)firstCard secondCard:(SetCard*)secondCard        //this is pretty obvious
{
    NSMutableString * returnstring = [NSMutableString stringWithString:@"Duplicate Cards!"];      //just set a string and change it if something else comes up

    if ((! [self.face isEqualToString: firstCard.face]) && (! [self.face isEqualToString: secondCard.face]) && (! [firstCard.face isEqualToString: secondCard.face]))
    {
        returnstring = [NSMutableString stringWithString:@"Found a set!"];

        if (! [self.character isEqualToString:firstCard.character]){
            if ([self.character isEqualToString:secondCard.character] || [firstCard.character isEqualToString:secondCard.character]){
                returnstring = [NSMutableString stringWithString:@"Characters Do Not Match!"];}
        }else{
            if (! [firstCard.character isEqualToString:secondCard.character]){
                returnstring = [NSMutableString stringWithString:@"Characters Do Not Match!"];}
        }

        if ( self.fill != firstCard.fill ){
            if ((self.fill == secondCard.fill) || (firstCard.fill == secondCard.fill)){
                returnstring = [NSMutableString stringWithString:@"Fills Do Not Match!"];}
        }else{
            if (firstCard.fill != secondCard.fill){
                returnstring = [NSMutableString stringWithString:@"Fills Do Not Match!"];}
        }
    
        if ( self.color != firstCard.color ){
            if ((self.color == secondCard.color) || (firstCard.color == secondCard.color)){
                returnstring = [NSMutableString stringWithString:@"Colors Do Not Match!"];}
        }else{
            if (firstCard.color != secondCard.color){
                returnstring = [NSMutableString stringWithString:@"Colors Do Not Match!"];}
        }
    
        if ( self.number != firstCard.number ){
            if ((self.number == secondCard.number) || (firstCard.number == secondCard.number)){
                returnstring = [NSMutableString stringWithString:@"# of Characters Do Not Match!"];}
        }else{
            if (firstCard.number != secondCard.number){
                returnstring = [NSMutableString stringWithString:@"# of Characters Do Not Match!"];}
        }
    }
    
    return returnstring;        // I just send back a string to be checked
}
@end
