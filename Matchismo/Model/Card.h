//
//  Card.h
//  Matchismo
//
//  Created by Mike Halliday on 2/5/13.
//  Copyright (c) 2013 Mike Halliday. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Card : NSObject

// generic card has what properties?

@property (nonatomic, strong) NSString * face;
@property (nonatomic, getter = isFaceUp) BOOL faceUp;
@property (nonatomic, getter = isPlayable) BOOL playable;

//generic cards can do what?

- (int) checkIfMatchWith: (Card*) otherCard;
- (void) flipCard;
- (void)setPlayable:(BOOL)plaable ;
@end
