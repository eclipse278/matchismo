//
//  SetCardDeck.h
//  Matchismo
//
//  Created by Mike Halliday on 2/10/13.
//  Copyright (c) 2013 Mike Halliday. All rights reserved.
//

#import "Deck.h"
#import "SetCard.h"

@interface SetCardDeck : Deck

- (id) initWithSetCards;

@end
