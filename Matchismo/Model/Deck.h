//
//  NewDeck.h
//  Matchismo
//
//  Created by Mike Halliday on 2/5/13.
//  Copyright (c) 2013 Mike Halliday. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Card.h"

@interface Deck : NSObject

@property (readonly, nonatomic) NSMutableArray * cards;   //of type PlayingCards

- (void) shuffleDeck;
- (id) initWithPlayingCards;
- (id)initFromArrayofCards:(NSArray*)theArray;
- (int) count;
- (id) DrawCard;
- (id)showCardatIndex:(int)index;
- (void) addCard:(Card*)theCard atIndex:(int)index;
- (NSString*) desc;
@end
