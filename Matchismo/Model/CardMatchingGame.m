//
//  CardMatchingGame.m
//  Matchismo
//
//  Created by Mike Halliday on 2/11/13.
//  Copyright (c) 2013 Mike Halliday. All rights reserved.
//

#import "CardMatchingGame.h"

@interface CardMatchingGame()
@property (readwrite, nonatomic) int score;         //score needs to be writeable in private
@end

@implementation CardMatchingGame

-(id) init{
    self = [super init];
    if (self){
        _deck = [[Deck alloc]init];
        _deckInPlay = [[Deck alloc]init];
        _discarded = [[Deck alloc]init];
    }
    return self;
}

-(id)initUsingDeck:(Deck *)initingDeck{
    
    self = [self init];
    
    if (self){
        _deck = initingDeck;
    }
    return self;
}

-(void) restartWithDeck:(Deck*)newDeck
{
    _deck = newDeck;
    _deckInPlay = [[Deck alloc]init];
    _discarded = [[Deck alloc]init];
    _score = 0;
}

-(Deck *)deckInPlay
{
    if (! _deckInPlay)
        _deckInPlay = [[Deck alloc]init];
    return _deckInPlay;
}

-(NSArray*) dealOutNumOfCards:(int)numofcards
{
    NSMutableArray * theCards = [[NSMutableArray alloc] initWithCapacity:numofcards];
    
    if (numofcards){
        Card *aCard = [[Card alloc]init];
        for (int i=0;i<numofcards;i++)
        {
            aCard = [self.deck DrawCard];
            [theCards addObject:aCard];
            [self.deckInPlay addCard:aCard atIndex:[self.deckInPlay count]];  //make sure we store the card as "inPlay"
        }
    }
    return [NSArray arrayWithArray:theCards];
}

-(void)flipCardatIndex:(int)index{
    if ([self isCardPlayableAtIndex:index])
    {
        [[self.deckInPlay.cards objectAtIndex:index] flipCard];
        
        if (   [[self.deckInPlay.cards objectAtIndex:index] isFaceUp]   )
            self.score--;
        
        //Check if any other cards are also flipped
        
        int checkindex = 0;
        for (Card* checkCard in self.deckInPlay.cards)
        {
            if (! [ [self.deckInPlay.cards objectAtIndex:index] isEqual:checkCard] )
            {
                if ( checkCard.isPlayable && [self.deckInPlay.cards[index] isPlayable] )
                {
                    if ( [checkCard isFaceUp] && [[self.deckInPlay.cards objectAtIndex:index] isFaceUp   ])
                    {
                        if ([self compareCard:[self.deckInPlay.cards objectAtIndex:index] withCard: checkCard] )
                        {
                            [self.deckInPlay.cards[index] setPlayable:NO];
                            [self.deckInPlay.cards[checkindex] setPlayable:NO];
                        }else{
                            //                    [self.deckInPlay.cards[index] flipCard];
                            [self.deckInPlay.cards[checkindex] flipCard];
                        }
                    }
                }
            }
            checkindex++;
        }
    }
}


-(BOOL)isCardPlayableAtIndex:(int)index
{
    return [self.deck.cards[index] isPlayable];
}

-(void)changePlayableStateforCardAtIndex:(int)index
{
    [self.deckInPlay.cards[index] setPlayable:NO];
}

-(int) compareCard:(Card *)cardOne withCard:(Card *)cardTwo
{
    int points = [cardOne checkIfMatchWith:cardTwo];
    self.score += points;
    return points;
}

@end
